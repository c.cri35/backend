const { Datastore } = require("@google-cloud/datastore");
const moment = require("moment");

const datastore = new Datastore({
  projectId: "flawless-snow-334314",
  keyFilename: "./database_service_account.json",
});

const kindName = "scheduled-tasks";

/**
 * Get all scheduled tasks.
 * https://us-east1-flawless-snow-334314.cloudfunctions.net/getScheduledTasks
 */
exports.getScheduledTasks = (req, res) => {
  const query = datastore.createQuery(kindName);
  res.type("application/json");
  res.set("Access-Control-Allow-Origin", "*");

  if (req.method === "OPTIONS") {
    // Send response to OPTIONS requests
    res.set("Access-Control-Allow-Methods", "GET");
    res.set("Access-Control-Allow-Headers", "Content-Type");
    res.set("Access-Control-Max-Age", "3600");
    res.status(204).send("");
    return;
  }

  datastore
    .runQuery(query)
    .then((tasks) => {
      res.status(200).send(JSON.stringify(tasks[0]));
    })
    .catch((err) => {
      res.status(500).send(JSON.stringify(err));
    });
};

/**
 * Add a scheduled task.
 * https://us-east1-flawless-snow-334314.cloudfunctions.net/addScheduledTask
 */
exports.addScheduledTask = (req, res) => {
  let scheduledTask = {
    start_date: req.body.start_date,
    end_date: req.body.end_date,
  };

  res.type("application/json");
  res.set("Access-Control-Allow-Origin", "*");

  if (req.method === "OPTIONS") {
    // Send response to OPTIONS requests
    res.set("Access-Control-Allow-Methods", "POST");
    res.set("Access-Control-Allow-Headers", "Content-Type");
    res.set("Access-Control-Max-Age", "3600");
    res.status(204).send("");
    return;
  }

  if (!scheduledTask.end_date || !scheduledTask.start_date) {
    res
      .status(500)
      .send(JSON.stringify({ message: "Must specify end and start date." }));
    return;
  }
  if (
    !moment(scheduledTask.start_date).isValid() ||
    !moment(scheduledTask.end_date).isValid()
  ) {
    res.status(500).send(JSON.stringify({ message: "Invalid date object." }));
    return;
  }

  datastore
    .save({
      key: datastore.key(kindName),
      data: scheduledTask,
    })
    .then((response) => {
      console.log(response);
      res.status(200).send(JSON.stringify(scheduledTask));
    })
    .catch((err) => {
      res.status(500).send({ message: JSON.stringify(err) });
    });
};
