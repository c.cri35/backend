import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./User";

@Entity()
export class RecognitionResult {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "bytea",
  })
  image: string;

  @Column()
  size: number;

  @Column()
  name: string;

  @Column()
  result: number;

  @Column()
  @ManyToOne((type) => User)
  userId: number;
}
