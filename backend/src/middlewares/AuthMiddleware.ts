import { NextFunction, Request, Response } from "express";

var jwt = require("jsonwebtoken");

export function needsAuth(req: Request, res: Response, next: NextFunction) {
  try {
    let token: String = req.headers.authorization.split(" ")[1];

    if (!jwt.verify(token, process.env.SECRET_KEY)) {
      res.sendStatus(401);
      return;
    }
  } catch (error) {
    console.log("Authorization Header not present or not the right format");
    res.sendStatus(401);
    return;
  }

  return next();
}
