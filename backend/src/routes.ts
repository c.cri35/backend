import { NeuralNetController } from "./controller/NeuralNetController";
import { RecognitionController } from "./controller/RecognitionController";
import { UserController } from "./controller/UserController";
import { needsAuth } from "./middlewares/AuthMiddleware";
import {
  checkExistingUserId,
  checkValidUserBodySave,
  checkValidUserBodyLogin,
} from "./middlewares/ValidationMiddleware";

export const Routes = [
  {
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
    middlewares: [needsAuth],
  },
  {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    middlewares: [needsAuth, checkExistingUserId],
  },
  {
    method: "put",
    route: "/users/:id",
    controller: UserController,
    action: "update",
    middlewares: [needsAuth, checkExistingUserId],
  },
  {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
    middlewares: [needsAuth, checkValidUserBodySave],
  },
  {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    middlewares: [needsAuth, checkExistingUserId],
  },
  {
    method: "post",
    route: "/register",
    controller: UserController,
    action: "register",
    middlewares: [checkValidUserBodySave],
  },
  {
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login",
    middlewares: [checkValidUserBodyLogin],
  },
  {
    method: "post",
    route: "/train",
    controller: NeuralNetController,
    action: "train",
  },
  {
    method: "post",
    route: "/evaluate",
    controller: NeuralNetController,
    action: "evaluate",
  },
  {
    method: "get",
    route: "/getRecognitionResults/:id",
    controller: RecognitionController,
    action: "getAllByUser",
    middlewares: [checkExistingUserId],
  },
];
