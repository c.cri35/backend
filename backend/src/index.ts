import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as expressFileUpload from "express-fileupload";
import { Router, Request, Response } from "express";
import { Routes } from "./routes";
import * as cors from "cors";

createConnection()
  .then(async (connection) => {
    // create express app
    const app = express();

    app.use(cors());
    app.use(expressFileUpload());
    app.use(bodyParser.json());

    var router = Router();

    // a global logging middleware
    router.use("/", (req: Request, res: Response, next: Function) => {
      console.log(`[${req.method}]: ${req.originalUrl}`);
      next();
    });

    // register express routes from defined application routes
    Routes.forEach((route) => {
      //if present, register middlewares in the order they were given
      if (route.middlewares) {
        route.middlewares.forEach((middleware) => {
          (router as any)[route.method](route.route, middleware);
        });
      }
      (router as any)[route.method](
        route.route,
        (req: Request, res: Response, next: Function) => {
          const result = new (route.controller as any)()[route.action](
            req,
            res,
            next
          );
          if (result instanceof Promise) {
            result.then((result) =>
              result !== null && result !== undefined
                ? res.send(result)
                : undefined
            );
          } else if (result !== null && result !== undefined) {
            res.json(result);
          }
        }
      );
    });

    app.use("/", router);

    const result = require("dotenv").config();

    if (result.error) {
      throw result.error;
    }

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
    // await connection.manager.save(
    //   connection.manager.create(User, {
    //     email: "timber@gmail.com",
    //     userName: "timber26",
    //     password: "Saw",
    //   })
    // );
    // await connection.manager.save(
    //   connection.manager.create(User, {
    //     email: "phantom@gmail.com",
    //     userName: "Assassin78",
    //     password: "24",
    //   })
    // );

    console.log(
      "Express server has started on port 3001. Open http://localhost:3001/users to see results"
    );
  })
  .catch((error) => console.log(error));
