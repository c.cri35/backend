export const MODEL_DIRECTORY = "file://./src/tensorflow/model";
export const MODEL_PATH = "file://./src/tensorflow/model/model.json";

export function toArrayBuffer(buf: Buffer) {
  var ab = new ArrayBuffer(buf.length);
  var view = new Uint8Array(ab);
  for (var i = 0; i < buf.length; ++i) {
    view[i] = buf[i];
  }
  return ab;
}
