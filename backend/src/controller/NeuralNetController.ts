import { NextFunction, Request, Response } from "express";
import { MODEL_DIRECTORY, toArrayBuffer } from "../utils/tensorflow_utils";
import { MnistData } from "./../tensorflow/data";
import { getModel, train, doPrediction } from "./../tensorflow/script";
import { RecognitionController } from "./RecognitionController";

export class NeuralNetController {
  async train(request: Request, response: Response, next: NextFunction) {
    const data = new MnistData();
    await data.load();
    const trainResults = await train(getModel(), data);
    await trainResults.model.save(MODEL_DIRECTORY);
    return trainResults.trainResults;
  }

  async evaluate(request, response: Response, next: NextFunction) {
    let arrayBuffer = toArrayBuffer(request.files.image.data);
    console.log(request.files.image.data);
    const prediction = (await doPrediction(arrayBuffer))[0];
    request.body.result = prediction;
    new RecognitionController().save(request, response, next);
    response.sendStatus(200);
    return prediction;
  }
}
