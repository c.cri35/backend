import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { RecognitionResult } from "../entity/RecognitionResult";

export class RecognitionController {
  private recognitionRepository = getRepository(RecognitionResult);

  async save(request, response: Response, next: NextFunction) {
    let recognitionResult = {
      image: request.files.image.data,
      size: request.body.size,
      name: request.body.name,
      result: request.body.result,
      userId: request.body.userId,
    };
    return this.recognitionRepository.save(recognitionResult);
  }

  async getAllByUser(request: Request, response: Response, next: NextFunction) {
    return this.recognitionRepository.find({
      where: [{ userId: request.params.id }],
    });
  }
}
