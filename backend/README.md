# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `docker run -v $PWD:/app -w /app -it node:16 npm i`
2. Run `npm i` command
3. Setup database settings inside `ormconfig.json` file
4. Run `npm start` command
